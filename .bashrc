#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return



#PS1='[\u@\h \W]\$ '
PS1='\[\e[0;1;38;5;196m\]\u\[\e[0;38;5;244m\]@\[\e[0;2;32m\]\h\[\e[0;2;32m\]|\[\e[0;33m\]\W\n\[\e[0;96m\]\$\[\e[0m\] \[\e[0m\]'


function find_largest_files(){
	du -h -x -s -- * | sort -r -h | head -20;
}

if [ -f ~/.bash_aliases ]; then
        source ~/.bash_aliases
fi
neofetch
