#
# ~/.bash_aliases
#


alias ls='ls --color=auto'
alias la='ls -a --color=auto'
alias ll='ls -alF'
alias lt='ls --human-readable --size -1 -S --classify'
alias gh='history|grep'
alias cpv='rsync -ah --info=progress2'
alias ping='ping -c 5'
alias config='/usr/bin/git --git-dir=/home/mitchell/.cfg/ --work-tree=/home/mitchell'
